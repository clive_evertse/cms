<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Delete User</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assests/css/style.css" media="screen" type="text/css" />
	</head>
	<body>
		<div id="content">
			<h1>Delete A user</h1>
			<a href="<?php echo base_url();?>site/home">Back</a>
			<p class = "isa_warning">Delete <?php echo $user['name'].' '.$user['surname'];?> from system? </p>
			<a href = "<?php echo base_url();?>user/remove_user/<?php echo $user['id'];?>">Delete User</a>
		</div>
	</body>
</html>
