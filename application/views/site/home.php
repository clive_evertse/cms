<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
		<title>Home</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assests/css/style.css" media="screen" type="text/css" />
	</head>
	<body>
		<div id="content">
			<h1>Users</h1> <div align = "right"><a href = "<?php echo base_url();?>login/logout" >Logout</a></div>
			<p><a href="<?php echo base_url();?>user/add/">Add User</a></p>
<?php
if (isset($action))
	{
		if($action == 'edited')
		{
			echo '<p class = "isa_success"> User has been successfully Edited! </p>';
		}
		if($action == 'deleted')
		{
			echo '<p class = "isa_success"> User has been successfully Deleted! </p>';
		}
		
	}?>
			<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Surname</th>
					<th>Email</th>
					<th>Last Active</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($users['records'] as $user): ?>
				<tr>
				<td><?php echo $user['name'];?></td>
				<td><?php echo $user['surname'];?></td>
				<td><?php echo $user['email'];?></td>
				<td><?php echo $user['last_activity'];?></td>
				<?php if ($user['role'] != 'admin')
					{
						echo '<td><a href="'.base_url().'user/delete_user/'.$user['id'].'">Delete</a><a href="'. base_url().'user/edit_user/'. $user['id'].'"> Edit</a></td>';
					}
				?>
				</tr>
			<?php endforeach; ?>
			  </tbody> 
			</table>
		</div>
	</body>
</html>
