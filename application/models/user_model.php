<?php

class User_model extends CI_Model {

	//Validates if user exists
	function validate($email,$password)
	{
		$this->db->where('email',$email );
		$this->db->where('password', md5($password));
		$query = $this->db->get('user');
		
		if($query->num_rows == 1)
		{
			return true;
		}
		
	}
	//Get specific user details
	function get_user($user_id)
	{
		$this->db->where('id',$user_id);
		return $this->db->select('id,name,surname,email')->from('user')->get()->row_array();
	}
	//Gets all user from user table
	function get_all_users()
	{
		$query = $this->db->select('id,name,surname,email,last_activity,role')->from('user')->get();
		if($query->num_rows >= 1)
		{
			$record_count = $query->num_rows;
			$data['records'] = $query->result_array();
			$data['record_count'] = $record_count;
			return $data;
		}
	}
	
	//Inserts user into user table
	function create_user($name,$surname,$email,$password)
	{
		$this->db->set('name', $name);
		$this->db->set('surname', $surname);
		$this->db->set('email', $email);
		$this->db->set('password', md5($password));
		$this->db->set('role', 'user');
		$this->db->set('last_activity', date('Y-m-d'));
		
		$this->db->insert('user');
	}
	
	//Checks what fields are being sent, and updates them to user table
	function edit_user($user_data)
	{
		$this->db->where('id',$user_data['id']);
		if ($user_data['name'] != '')
			{
				$this->db->set('name',$user_data['name']);
			}
		if ($user_data['surname'] != '')
			{
				$this->db->set('surname',$user_data['surname']);
			}
		if ($user_data['email'] != '')
			{
				$this->db->set('email',$user_data['email']);
			}
		if ($user_data['password'] != '')
			{
				$this->db->set('password',md5($user_data['password']));
			}
			
		$this->db->update('user');
	}
	
	//Removes user from user table
	function delete_user($user_id)
	{
		$this->db->where('id',$user_id);
		$this->db->delete('user');
	}
	
	//Updates User activity
	function update_user_activity()
	{
		$update_member_activity = array (
		'last_activity' => date('Y-m-d')
		);
		$this->db->where('email',$this->session->userdata('email'));
		$this->db->update('user',$update_member_activity);
	}
	
}